FROM nginxinc/nginx-unprivileged:1-alpine

# Copy in our configuration files
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

# Copy our entry point shell script
COPY ./entrypoint.sh ./entrypoint.sh

# Make that file executable
RUN chmod +x /entrypoint.sh

USER nginx

# Set our default CMD for when the container starts
CMD ["/entrypoint.sh"]

