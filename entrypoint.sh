#!/bin/sh

# Return a failure if any of the following lines fail
set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Start the service
nginx -g 'daemon off;'